package repositories

import (
	"context"
	"database/sql"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
)

// DBTransaction contract database transaction
type DBTransaction interface {
	ExecTX(ctx context.Context, options *sql.TxOptions, fn func(context.Context, StoreTX) (int64, error)) (int64, error)
}

// StoreTX data store transaction contract
type StoreTX interface {
	// Create your function contract here
}

type Client interface {
	Get(context.Context, entity.ClientApiKey) error
}
