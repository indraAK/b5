// Package router
package router

import (
	"context"
	"encoding/json"
	"net/http"
	"runtime/debug"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/bootstrap"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/handler"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/middleware"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/ucase"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/routerkit"

	//"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/mariadb"
	//"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	//"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/ucase/example"

	ucaseContract "gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/ucase/contract"
)

type router struct {
	config *appctx.Config
	router *routerkit.Router
}

// NewRouter initialize new router wil return Router Interface
func NewRouter(cfg *appctx.Config) Router {
	bootstrap.RegistryMessage()
	bootstrap.RegistryLogger(cfg)

	return &router{
		config: cfg,
		router: routerkit.NewRouter(routerkit.WithServiceName(cfg.App.AppName)),
	}
}

func (rtr *router) handle(hfn httpHandlerFunc, svc ucaseContract.UseCase, mdws ...middleware.MiddlewareFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			err := recover()
			if err != nil {
				w.Header().Set(consts.HeaderContentTypeKey, consts.HeaderContentTypeJSON)
				w.WriteHeader(http.StatusInternalServerError)
				res := appctx.Response{
					Code: consts.CodeInternalServerError,
				}

				res.GenerateMessage()
				logger.Error(logger.MessageFormat("error %v", string(debug.Stack())))
				json.NewEncoder(w).Encode(res)
				return
			}
		}()

		ctx := context.WithValue(r.Context(), "access", map[string]interface{}{
			"path":      r.URL.Path,
			"remote_ip": r.RemoteAddr,
			"method":    r.Method,
		})

		req := r.WithContext(ctx)

		if status := middleware.FilterFunc(rtr.config, req, mdws); status != 200 {
			rtr.response(w, appctx.Response{
				Code: status,
			})

			return
		}

		resp := hfn(req, svc, rtr.config)
		resp.Lang = rtr.defaultLang(req.Header.Get(consts.HeaderLanguageKey))
		rtr.response(w, resp)
	}
}

// response prints as a json and formatted string for DGP legacy
func (rtr *router) response(w http.ResponseWriter, resp appctx.Response) {

	w.Header().Set(consts.HeaderContentTypeKey, consts.HeaderContentTypeJSON)

	defer func() {
		resp.GenerateMessage()
		w.WriteHeader(resp.GetCode())
		json.NewEncoder(w).Encode(resp)
	}()

	return

}

// Route preparing http router and will return mux router object
func (rtr *router) Route() *routerkit.Router {

	root := rtr.router.PathPrefix("/").Subrouter()
	ex := root.PathPrefix("/external").Subrouter()
	liveness := root.PathPrefix("/").Subrouter()
	//ad := root.PathPrefix("/admin").Subrouter()

	// open tracer setup
	bootstrap.RegistryOpenTracing(rtr.config)

	//db := bootstrap.RegistryMariaMasterSlave(rtr.config.WriteDB, rtr.config.ReadDB, rtr.config.App.Timezone)

	// use case
	healthy := ucase.NewHealthCheck()
	getToken := ucase.NewGetToken()
	//generateJWT := middleware.CreateJWT()

	// healthy
	liveness.HandleFunc("/liveness", rtr.handle(
		handler.HttpRequest,
		healthy,
	)).Methods(http.MethodGet)

	greeting := ucase.NewGreeting()

	root.HandleFunc("/greeting/{name}", rtr.handle(
		handler.HttpRequest,
		greeting,
	)).Methods(http.MethodPost)

	ex.HandleFunc("/generate/token", rtr.handle(
		handler.HttpRequest,
		getToken,
	)).Methods(http.MethodGet)

	return rtr.router

}

func (rtr *router) defaultLang(l string) string {

	if len(l) == 0 {
		return rtr.config.App.DefaultLang
	}

	return l
}
