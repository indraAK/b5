package middleware

import (
	"crypto/hmac"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

var SECRET = []byte("indra3325111811980001")

func CreateJWT() (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)

	claims["exp"] = time.Now().Add(time.Hour * 2).Unix()

	tokenStr, err := token.SignedString(SECRET)

	if err != nil {
		fmt.Println(err.Error())
		return "", err
	}

	return tokenStr, nil
}

func ValidateHmac(signatureClient string, serverSignature string) bool {
	cekHmac := hmac.Equal([]byte(signatureClient), []byte(serverSignature))

	return cekHmac
}

//func ValidateJWT(next func(w http.ResponseWriter, r *http.Request)) http.Handler {
//	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//		if r.Header["Authorization"] != nil {
//
//			authHeader := r.Header["Authorization"][0]
//			if !strings.Contains(authHeader, "Bearer") {
//				w.WriteHeader(http.StatusUnauthorized)
//				w.Write([]byte("Invalid Token"))
//			}
//
//			arrayToken := strings.Split(authHeader, " ")
//
//			tokenString := ""
//			if len(arrayToken) == 2 {
//				tokenString = arrayToken[1]
//			}
//
//			token, err := jwt.Parse(tokenString, func(t *jwt.Token) (interface{}, error) {
//				_, ok := t.Method.(*jwt.SigningMethodHMAC)
//				if !ok {
//					w.WriteHeader(http.StatusUnauthorized)
//					w.Write([]byte("Unauthorized"))
//				}
//				return SECRET, nil
//			})
//
//			if err != nil {
//				w.WriteHeader(http.StatusUnauthorized)
//				w.Write([]byte("Unauthorized " + err.Error()))
//			}
//			if token.Valid {
//				next(w, r)
//			}
//		} else {
//			w.WriteHeader(http.StatusUnauthorized)
//			w.Write([]byte("Unauthorized"))
//		}
//	})
//}
