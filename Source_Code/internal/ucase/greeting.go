package ucase

import (
	"encoding/json"
	"fmt"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"net/http"
)

type greeting struct {
}

func (greeting) Serve(data *appctx.Data) appctx.Response {
	//name := data.Request.URL.Query().Get("name")

	//params := mux.Vars(data.Request)
	//name := params["name"]

	//name := data.Request.FormValue("name")
	//f, header, err := data.Request.FormFile("image")
	//if err != nil {
	//	//DO ERROR HANDLING
	//}
	//defer f.Close()
	//
	//buf := new(bytes.Buffer)
	//buf.ReadFrom(f)
	//
	//err = os.WriteFile(header.Filename, buf.Bytes(), 0644)
	//if err != nil {
	//	//Do ERROR HANDLING HERE
	//}
	var payload struct {
		Name string `json:"name"`
	}

	err := json.NewDecoder(data.Request.Body).Decode(&payload)
	if err != nil {
		//DO ERROR HANDLING HERE
	}
	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithData(map[string]string{
			"message": fmt.Sprintf("Hello %s!", payload.Name),
		})
}

func NewGreeting() *greeting {
	return &greeting{}
}
