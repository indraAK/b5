package ucase

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/google/uuid"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/middleware"
)

type getToken struct {
}

func NewGetToken() *getToken {
	return &getToken{}
}

func (gt getToken) Serve(data *appctx.Data) appctx.Response {
	if data.Request.Header["Signature"] == nil {
		return *appctx.NewResponse().
			WithStatus("failed").
			WithEntity("generateToken").
			WithState("generateTokenFailed").
			WithCode(http.StatusInternalServerError).
			WithMessage("Signature Not Found")
	}

	if data.Request.Header["Timestamp"] == nil {
		return *appctx.NewResponse().
			WithStatus("failed").
			WithEntity("generateToken").
			WithState("generateTokenFailed").
			WithCode(http.StatusInternalServerError).
			WithMessage("Timestamp Not Found")
	}

	if data.Request.Header["Xapi"] == nil {
		return *appctx.NewResponse().
			WithStatus("failed").
			WithEntity("generateToken").
			WithState("generateTokenFailed").
			WithCode(http.StatusInternalServerError).
			WithMessage("x_api_key Not Found")
	}

	Timestamp := data.Request.Header["Timestamp"][0]
	x_api_key_id := data.Request.Header["Xapi"][0]

	body, err := ioutil.ReadAll(data.Request.Body)
	if err != nil {
		panic(err)
	}

	var bodyRaw = md5.Sum([]byte(body))
	finalBody := string(bodyRaw[:])
	var body_md5 = base64.StdEncoding.EncodeToString([]byte(finalBody))

	hmac_signature := Timestamp + ":" + x_api_key_id + ":" + data.Request.Method + ":" + body_md5

	secret := "indra3325111811980001"

	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(hmac_signature))
	var hmac_base64 = base64.StdEncoding.EncodeToString(h.Sum(nil))

	signature := "#" + x_api_key_id + ":#" + hmac_base64
	serverSignature := base64.StdEncoding.EncodeToString([]byte(signature))

	signatureClient := data.Request.Header["Signature"][0]

	Hmac := middleware.ValidateHmac(signatureClient, serverSignature)

	fmt.Println("=============Test HMAC==============")
	fmt.Println("signature server :", serverSignature)
	fmt.Println("========================================")
	fmt.Println("signature client :", signatureClient)
	fmt.Println("========================================")
	fmt.Println("is HMAC Equal : ", Hmac)
	fmt.Println("========================================")

	if !Hmac {
		return *appctx.NewResponse().
			WithStatus("failed").
			WithEntity("generateToken").
			WithState("generateTokenFailed").
			WithCode(http.StatusInternalServerError).
			WithMessage("Signature Invalid")
	}

	token, err := middleware.CreateJWT()

	if err != nil {
		return *appctx.NewResponse().
			WithStatus("error generating token").
			WithCode(http.StatusInternalServerError).
			WithError(err)
	}

	meta := map[string]interface{}{
		"transaction_id": uuid.New(),
	}

	dataResponse := map[string]interface{}{
		"type":       "bearer",
		"token":      token,
		"expired_at": time.Now().Add(time.Hour * 2),
	}

	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithStatus("Success").
		WithEntity("generateToken").
		WithState("generateTokenSuccess").
		WithMeta(meta).
		WithData(dataResponse)
}
