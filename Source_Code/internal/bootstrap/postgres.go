package bootstrap

import (
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/mariadb"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/postgres"
	"time"

	config "gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
)

func RegistryPostgres(cfg *config.Database) mariadb.Adapter {
	db, err := postgres.NewAdapter(&postgres.Config{
		Host:         cfg.Host,
		Name:         cfg.Name,
		Password:     cfg.Pass,
		Port:         cfg.Port,
		User:         cfg.User,
		Timeout:      time.Duration(cfg.TimeoutSecond) * time.Second,
		MaxOpenConns: cfg.MaxOpen,
		MaxIdleConns: cfg.MaxIdle,
		MaxLifetime:  time.Duration(cfg.MaxLifeTimeMS) * time.Millisecond,
	})

	if err != nil {
		logger.Fatal(
			err,
			logger.EventName("db"),
			logger.Any("host", cfg.Host),
			logger.Any("port", cfg.Port),
		)
	}

	return db
}
